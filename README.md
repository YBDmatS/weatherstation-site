# [[ WeatherStation - WEBSITE ]]



## Description

Developed for a Weather Station student project.

The website is done with HTML/CSS/JS and show the probe's reports.

The graph is auto refreshed when new reports are stored into the database.

![Schema](Schema.png)

To see the whole project, have a look to the 2 other repositories :

https://gitlab.com/YBDmatS/weatherstation-esp01

https://gitlab.com/YBDmatS/weatherstation-api



## Visual



![WeatherSite](WeatherSite.png)

